using Terraria.ID;
using Terraria.ModLoader;

namespace Ceraturesmod.Items
{
	public class Slimegun : ModItem
	{
		public override void SetStaticDefaults()
		{
			DisplayName.SetDefault("Slimegun");
			Tooltip.SetDefault("Nobody likes slime.");
		}
		public override void SetDefaults()
		{
			item.damage = 4;
            item.noMelee = true;
            item.ranged = true;
			item.width = 30;
			item.height = 30;
			item.useTime = 20;
			item.useAnimation = 20;
			item.useStyle = 5;
			item.knockBack = 0;
			item.value = 10000;
			item.rare = 2;
			item.UseSound = SoundID.Item1;
			item.autoReuse = true;
            item.shoot = 10;
            item.shootSpeed = 16f;
            item.useAmmo = ItemID.Gel;
		}
	}
}
