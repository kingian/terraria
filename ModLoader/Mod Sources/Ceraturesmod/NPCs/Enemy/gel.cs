using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace Ceraturesmod.NPCs.Enemy
{
    public class gel : ModNPC
    {
        public override void SetStaticDefaults()
        {
            DisplayName.SetDefault("Gel");
        }

        public override void  SetDefaults()
        {
            npc.width = 120;
            npc.height = 120;
            npc.lifeMax = 25;
            npc.damage = 10;
            npc.noGravity = true;
            npc.noTileCollide = true;
        }

        public override bool PreAI()
        {
            npc.TargetClosest(true);

            double targetX = Main.player[npc.target].position.X + Main.player[npc.target].width*0.5;
            double targetY = Main.player[npc.target].position.Y + Main.player[npc.target].height * 0.5;
            targetX -= npc.width * 0.5;
            targetY -= npc.height * 0.5;
            npc.position.X = (float)targetX;
            npc.position.Y = (float)targetY;

            return false;
        }
    }
}