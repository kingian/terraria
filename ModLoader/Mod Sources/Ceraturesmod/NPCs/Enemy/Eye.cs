using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using System;

namespace Ceraturesmod.NPCs.Enemy
{
    public class Eye : ModNPC
    {
        public override void SetStaticDefaults()
        {
            DisplayName.SetDefault("Eye");
        }

        public override void  SetDefaults()
        {
            npc.width = 288;
            npc.height = 165;
            npc.lifeMax = 25;
            npc.damage = 10;
            npc.noGravity = true;
            npc.noTileCollide = true;
        }

        public override bool PreAI()
        {
            npc.TargetClosest(true);

            double targetX = Main.player[npc.target].position.X + Main.player[npc.target].width*0.5;
            double targetY = Main.player[npc.target].position.Y + Main.player[npc.target].height * 0.5;

            double npcX = npc.position.X + npc.width * 0.5;
            double npcY = npc.position.Y + npc.height * 0.5;
            Vector2 posTarget = new Vector2((float)targetX, (float)targetY);
            Vector2 posNpc = new Vector2((float)npcX, (float)npcY);

            float a, b, c;
            a = posNpc.X - posTarget.X;
            b = posNpc.Y - posTarget.Y;
            c = (float)Math.Sqrt(a * a + b * b);

            float deg = (float)Math.Asin(b / c);
            return false;
        }
    }
}