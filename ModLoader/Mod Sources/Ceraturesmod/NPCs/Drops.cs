using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace Ceraturesmod.NPCs
{
    public class ModGlobalNPC : GlobalNPC
    {
        public override void NPCLoot(NPC npc)
        {
            if (npc.type == 1)
                if (Main.rand.Next(20)==0)
                    Item.NewItem((int)npc.position.X, (int)npc.position.Y, npc.height, npc.width, mod.ItemType("Slimegun"));
        }
    }
}