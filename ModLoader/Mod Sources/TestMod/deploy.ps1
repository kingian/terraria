$modName = "TestMod"
Write-Host "+++++++++++++++++++++++Deploy $modName to Terraria Sources+++++++++++++++++++++++++++"
$bitbucket = $env:BITBUCKETS
$prjPath = "$bitbucket\terraria\src\$modName"
Write-Host "100:$prjPath"

$documentFolderPath = [environment]::getfolderpath("mydocuments")
$modSourcePath = "My Games\Terraria\ModLoader\Mod Sources"
$targetPath = "$documentFolderPath\$modSourcePath\$modName"

if (Test-Path $targetPath) { Remove-Item $targetPath -Recurse; }


#$copyFiles=@("*.doc", "*.csv")
#Get-ChildItem -recurse ($source) -include ($files) -File | Where-Object {$_.PSParentPath -notlike "*Program Files*" -and $_.PSParentPath -notlike "*Users*" -and $_.PSParentPath -notlike "*Windows*"}
$list = Get-ChildItem -path $prjPath -recurse -File | Where-Object {$_.PSParentPath -notlike "*bin*" -and $_.PSParentPath -notlike "*obj*" -and $_.PSParentPath -notlike "*.vs*"}
foreach ($file in $list){
    $fileSourcePath = $file.FullName
    #$relFilePath = $fileSourcePath.TrimStart($prjPath)
    $relFilePath = $fileSourcePath.Substring($prjPath.Length+1)
    Write-Host "111:$relFilePath"
    $fileTargetPath = "$targetPath\$relFilePath"

    Write-Host "$fileSourcePath --> $fileTargetPath"
    New-Item -ItemType File -Path $fileTargetPath -Force | out-null
    Copy-Item $fileSourcePath $fileTargetPath -Recurse -Force

}
Write-Host "---------------------------$modName deployed-----------------"