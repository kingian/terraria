using Terraria.ID;
using Terraria.ModLoader;

namespace TestMod.Items
{
	public class Spitzhacke : ModItem
	{
		public override void SetStaticDefaults()
		{
			DisplayName.SetDefault("Spitzhacke");
			Tooltip.SetDefault("This is a modded sword 4.");
		}
		public override void SetDefaults()
		{
			item.damage = 6;
			item.melee = true;
			item.width = 40;
			item.height = 50;
			item.useTime = 30;
			item.useAnimation = 20;
            item.pick = 50;
			item.useStyle = 1;
			item.knockBack = 4;
			item.value = 10000;
			item.rare = 2;
			item.UseSound = SoundID.Item1;
			item.autoReuse = true;
            item.useTurn = true;
		}

		public override void AddRecipes()
		{
			ModRecipe recipe = new ModRecipe(mod);
            recipe.AddIngredient(ItemID.StoneBlock, 7);
            recipe.AddIngredient(ItemID.CopperBar, 2);
            recipe.AddTile(TileID.WorkBenches);
			recipe.SetResult(this);
			recipe.AddRecipe();
		}
	}
}
