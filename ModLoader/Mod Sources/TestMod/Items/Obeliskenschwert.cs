using Terraria.ID;
using Terraria.ModLoader;

namespace TestMod.Items
{
	public class Obeliskenschwert : ModItem
	{
		public override void SetStaticDefaults()
		{
			DisplayName.SetDefault("Obeliskenschwert");
			Tooltip.SetDefault("This is a good obelisk sword.");
		}
		public override void SetDefaults()
		{
			item.damage = 7;
			item.melee = true;
			item.width = 50;
			item.height = 50;
			item.useTime = 50;
			item.useAnimation = 20;
			item.useStyle = 3;
			item.knockBack = 6;
			item.value = 10000;
			item.rare = 1;
			item.UseSound = SoundID.Item1;
			item.autoReuse = false;
		}

		public override void AddRecipes()
		{
			ModRecipe recipe = new ModRecipe(mod);
            recipe.AddIngredient(ItemID.Obelisk, 3 );
            recipe.AddTile(TileID.WorkBenches);
			recipe.SetResult(this);
			recipe.AddRecipe();
		}
	}
}
