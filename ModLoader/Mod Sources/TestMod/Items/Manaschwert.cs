using Terraria.ID;
using Terraria.ModLoader;

namespace TestMod.Items
{
	public class Manaschwert : ModItem
	{
		public override void SetStaticDefaults()
		{
			DisplayName.SetDefault("Manaschwert");
			Tooltip.SetDefault("Ein Schwert das Mana braucht.");
		}
		public override void SetDefaults()
		{
			item.damage = 11;
			item.melee = true;
			item.width = 50;
			item.height = 50;
			item.useTime = 50;
			item.useAnimation = 20;
			item.useStyle = 1;
			item.knockBack = 7;
			item.value = 10000;
			item.rare = 1;
			item.UseSound = SoundID.Item1;
			item.autoReuse = true;
            item.mana = 1;
		}

		public override void AddRecipes()
		{
			ModRecipe recipe = new ModRecipe(mod);
            recipe.AddIngredient(ItemID.ManaCrystal, 1);
            recipe.AddIngredient(ItemID.Gel, 50);
            recipe.AddTile(TileID.Furnaces);
            recipe.SetResult(this);
            recipe.AddRecipe();
		}
	}
}
