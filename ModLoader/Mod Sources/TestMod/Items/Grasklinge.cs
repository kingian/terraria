using Terraria.ID;
using Terraria.ModLoader;

namespace TestMod.Items
{
	public class Grasklinge : ModItem
	{
		public override void SetStaticDefaults()
		{
			DisplayName.SetDefault("Unkrautklinge");
			Tooltip.SetDefault("This is a modded sword 3.");
		}
		public override void SetDefaults()
		{
			item.damage = 5;
			item.melee = true;
			item.width = 50;
			item.height = 50;
			item.useTime = 50;
			item.useAnimation = 20;
			item.useStyle = 1;
			item.knockBack = 4;
			item.value = 10000;
			item.rare = 1;
			item.UseSound = SoundID.Item1;
			item.autoReuse = true;
		}

		public override void AddRecipes()
		{
			ModRecipe recipe = new ModRecipe(mod);
            recipe.AddIngredient(ItemID.Sunflower, 3 );
            recipe.AddIngredient(ItemID.Acorn, 5);
            recipe.AddIngredient(ItemID.Wood, 2);
            recipe.AddTile(TileID.WorkBenches);
			recipe.SetResult(this);
			recipe.AddRecipe();
		}
	}
}
