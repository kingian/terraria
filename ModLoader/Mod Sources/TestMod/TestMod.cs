using Terraria.ModLoader;


//FAQ tModulLoader
//https://github.com/blushiemagic/tModLoader/wiki/Developing-with-Visual-Studio

//Tutorials
//tModLoader Sources
//https://github.com/blushiemagic/tModLoader/wiki/Basic-tModLoader-Modding-Guide
//https://forums.terraria.org/index.php?threads/tutorial-1-getting-started-with-tmodloader.44817/
//https://forums.terraria.org/index.php?threads/tutorial-3-items.44842/

//Modloader Doku
//http://blushiemagic.github.io/tModLoader/html/index.html

namespace TestMod
{
	class TestMod : Mod
	{
		public TestMod()
		{
            Properties = new ModProperties()
            {
                Autoload = true,
                AutoloadGores = true,
                AutoloadSounds = true
            };
        }
	}
}
