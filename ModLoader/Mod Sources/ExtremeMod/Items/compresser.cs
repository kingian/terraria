using Terraria.ID;
using Terraria.ModLoader;

namespace ExtremeMod.Items
{
    public class compresser : ModItem
    {
        public override void SetStaticDefaults()
        {
            DisplayName.SetDefault("Compresser");
            Tooltip.SetDefault("XXXXX");
        }

        public override void SetDefaults()
        {
            item.width = 28;
            item.height = 14;
            item.useTurn = true;
            item.autoReuse = true;
            item.useAnimation = 15;
            item.useTime = 10;
            item.useStyle = 1;
            item.consumable = true;
            item.value = 150;
            item.createTile = mod.TileType("compresser");
        }

        public override void AddRecipes()
        {
            ModRecipe recipe = new ModRecipe(mod);
            recipe.AddIngredient(ItemID.DirtBlock, 1);
            recipe.SetResult(this);
            recipe.AddRecipe();
        }
    }
}