using Terraria.ID;
using Terraria.ModLoader;

namespace ExtremeMod.Items.Blocks.Redstone
{
	public class Chestsender : ModItem
	{
		public override void SetStaticDefaults()
		{
			DisplayName.SetDefault("Chestsender");
			Tooltip.SetDefault("If you want to build a maschine you will like it.");
		}
		public override void SetDefaults()
		{
			item.width = 32;
			item.height = 32;
            item.maxStack = 99;
            item.useTurn = true;
            item.autoReuse = true;
            item.useAnimation = 15;
            item.useTime = 20;
            item.useStyle = 1;
            item.consumable = true;
            item.createTile = mod.TileType("Chestsenderblock");
        }

		public override void AddRecipes()
		{
			ModRecipe recipe = new ModRecipe(mod);
			recipe.AddIngredient(ItemID.DirtBlock, 10);
			recipe.AddTile(TileID.WorkBenches);
			recipe.SetResult(this);
			recipe.AddRecipe();
		}
	}
}
